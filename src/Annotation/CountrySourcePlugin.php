<?php

namespace Drupal\country_entity\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a Country source plugin item annotation object.
 *
 * @see \Drupal\country_entity\Plugin\CountrySourcePluginManager
 * @see plugin_api
 *
 * @Annotation
 */
class CountrySourcePlugin extends Plugin {


  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The label of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

  /**
   * The data source used to populate the country list.
   *
   * @var string
   */
  public $datasource;

}

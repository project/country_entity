<?php

namespace Drupal\country_entity\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ImportController.
 */
class ImportController extends ControllerBase {

  /**
   * Drupal\country_entity\CreateCountriesServiceInterface definition.
   *
   * @var \Drupal\country_entity\CreateCountriesServiceInterface
   */
  protected $countryEntityCreateCountries;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->countryEntityCreateCountries = $container->get('country_entity.create_countries');
    return $instance;
  }

  /**
   * Import.
   *
   * @return string
   *   Return Hello string.
   */
  public function import() {
    return [
      '#type' => 'markup',
      '#markup' => $this->t('Implement method: import'),
    ];
  }

}

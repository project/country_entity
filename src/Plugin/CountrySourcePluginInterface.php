<?php

namespace Drupal\country_entity\Plugin;

use Drupal\Component\Plugin\PluginInspectionInterface;

/**
 * Defines an interface for Country source plugin plugins.
 */
interface CountrySourcePluginInterface extends PluginInspectionInterface {

  /**
   * @return mixed
   */
  public function getSourceData();

  /**
   * @return mixed
   */
  public function getMappedFields();

  /**
   * @return mixed
   */
  public function getSkipFields();

}

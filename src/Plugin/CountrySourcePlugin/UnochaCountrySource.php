<?php

namespace Drupal\country_entity\Plugin\CountrySourcePlugin;

use Drupal\country_entity\Plugin\CountrySourcePluginBase;

/**
 * Provides a processing configuration plugin.
 *
 * @CountrySourcePlugin(
 *   id = "unocha_country_source",
 *   label = @Translation("Retrieves data from https://vocabulary.unocha.org/"),
 *   datasource = "http://vocabulary.unocha.org/json/beta-v3/countries.json",
 * )
 */
class UnochaCountrySource extends CountrySourcePluginBase {

  /**
   * @return array
   */
  public function getMappedFields() {
    $mappedFields = [
      'label_default' => 'name',
      'geolocation_lat' => 'latitude',
      'geolocation_lon' => 'longitude',
      'region_label_default' => 'continent',
      'region_code' => 'continent_code',
      'sub_region_label_default' => 'continent_sub_region',
      'sub_region_code' => 'continent_sub_region_code',
    ];
    $this->setMappedFields($mappedFields);
    return parent::getMappedFields();
  }

  /**
   * @return array
   */
  public function getSkipFields() {
    $skipFields = ['id'];
    $this->setSkipFields($skipFields);
    return parent::getSkipFields();
  }

}

<?php

namespace Drupal\country_entity\Plugin;

use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use GuzzleHttp\ClientInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base class for Country source plugin plugins.
 */
abstract class CountrySourcePluginBase extends PluginBase implements CountrySourcePluginInterface, ContainerFactoryPluginInterface {

  public $httpClient;

  public $mappedFields;

  public $skipFields;

  public $flattenedData;

  /**
   * EmbedCreateBase constructor.
   *
   * @param array $configuration
   * @param $plugin_id
   * @param $plugin_definition
   */
  public function __construct(array $configuration,
                              $plugin_id,
                              $plugin_definition,
                              ClientInterface $http_client) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->httpClient = $http_client;
  }

  /**
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   * @param array $configuration
   * @param string $plugin_id
   * @param mixed $plugin_definition
   * @return static
   */
  public static function create(ContainerInterface $container,
                                array $configuration,
                                $plugin_id,
                                $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('http_client')
    );
  }

  /**
   * @return array|mixed
   */
  public function getSourceData() {
    $response = json_decode($this->response(), TRUE);
    $this->setSourceData($response);
    return $this->flattenedData;
  }

  /**
   * @param $response
   */
  protected function setSourceData($response) {
    $this->flattenedData = $this->flattenData($response['data']);
  }

  /**
   * @return mixed
   */
  public function getMappedFields() {
    return $this->mappedFields;
  }

  /**
   * @param mixed $mappedFields
   */
  protected function setMappedFields($mappedFields): void {
    $this->mappedFields = $mappedFields;
  }

  /**
   * @return mixed
   */
  public function getSkipFields() {
    return $this->skipFields;
  }

  /**
   * @param mixed $skipFields
   */
  public function setSkipFields($skipFields): void {
    $this->skipFields = $skipFields;
  }

  /**
   * @param $data
   *
   * @return array
   */
  private function flattenData($data) {
    foreach ($data as $row) {
      $flattened[] = $this->arrayFlatten($row);
    }
    return $flattened;
  }

  /**
   * @param $array
   * @param array $return
   * @param null $newKey
   *
   * @return array
   */
  private function arrayFlatten($array, $return = [], $newKey = null) {
    foreach ($array AS $key => $value) {
      $key = ($newKey ? $newKey . '_' . $key : $key);
      $key = str_replace('-', '_', $key);
      if (is_array($value)) {
        $return = $this->arrayFlatten($value, $return, $key);
      }
      else {
        if (isset($value)) {
          $return[$key] = $value;
        }
      }
    }
    return $return;
  }

  /**
   * @return bool|string
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  private function response() {
    $datasource = $this->pluginDefinition['datasource'];
    $status = '';
    try {
      $request = $this->httpClient->request('GET', $datasource);
      $status = $request->getStatusCode();
      $transfer_success = $request->getBody()->getContents();
      return $transfer_success;
    }
    catch (RequestException $e) {
      $message = 'Lookup failed' . $e . '::' . $status;
      \Drupal::logger('Country Source')->error($message);
    }
    return FALSE;
  }

}

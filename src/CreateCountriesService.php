<?php

namespace Drupal\country_entity;

use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\country_entity\Plugin\CountrySourcePluginManager;

/**
 * Class CreateCountriesService
 *
 * @package Drupal\country_entity
 */
class CreateCountriesService implements CreateCountriesServiceInterface {

  /**
   * \Drupal\core\entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\core\entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * \Drupal\core\entity\EntityFieldManagerInterface definition.
   *
   * @var \Drupal\core\entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * \Drupal\country_entity\Plugin\CountrySourcePluginManager definition.
   *
   * @var \Drupal\country_entity\Plugin\CountrySourcePluginManager
   */
  protected $pluginManagerCountrySourcePlugin;

  /**
   * Constructs a new CreateCountriesService object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   * @param \Drupal\country_entity\Plugin\CountrySourcePluginManager $plugin_manager_country_source_plugin
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, EntityFieldManagerInterface $entity_field_manager, CountrySourcePluginManager $plugin_manager_country_source_plugin) {
    $this->entityTypeManager = $entity_type_manager;
    $this->entityFieldManager = $entity_field_manager;
    $this->pluginManagerCountrySourcePlugin = $plugin_manager_country_source_plugin;
  }

  /**
   * Search.
   *
   * @return string
   *   Return Hello string.
   */
  public function create($datasource) {

    $countryDataSet = $this->loadPlugin($datasource);

    if (empty($countryDataSet)) {
      return FALSE;
    }
    // List the fields available to the entity using entity field manager.
    $country_entity_fields = array_keys($this->entityFieldManager->getFieldStorageDefinitions('country_entity'));
    $i = 0;
    foreach ($countryDataSet['countries'] as $country) {
      $values = [];
      // Loop through the fields from entity field manager.
      foreach ($country_entity_fields as $key) {
        // With each field check that value is not available in fieldMapped.
        // If it is make sure the values is added using the mapped key.
        if ($skipResult = in_array($key, $countryDataSet['skipFields'])) {
          // Do Nothing.
        }
        elseif ($mappedResult = array_search($key, $countryDataSet['mappedFields'])) {
          if (isset($country[$mappedResult])) {
            $values[$key] = $country[$mappedResult];
          }
        }
        elseif (isset($country[$key])) {
          $values[$key] = $country[$key];
        }
      }
      if ($check_country = $this->entityTypeManager->getStorage('country_entity')
        ->loadByProperties($values)) {
        $notice = 'Country ' . $i . ' : ' . $country['label_english_short'] . ' was skipped';
      }
      else {
        $countryEntity = $this->entityTypeManager->getStorage('country_entity')->create($values);
        $countryEntity->save();
        $notice = 'Country ' . $i . ' : ' . $country['label_english_short'] . ' was created';
      }
      \Drupal::logger('Country Import')->notice($notice);
      $i++;

    }
    return TRUE;
  }

  /**
   * @return array
   */
  public function getPluginIds() {
    foreach ($this->pluginManagerCountrySourcePlugin->getDefinitions() as $countrySource) {
      $countrySourceId[] = $countrySource['id'];
    }
    return $countrySourceId;
  }

  /**
   * @param $datasource
   *
   * @return mixed
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  private function loadPlugin($datasource) {
    foreach ($this->getPluginIds() as $sourceId) {
      if ($datasource == $sourceId) {
        $plugin = $this->pluginManagerCountrySourcePlugin->createInstance($sourceId);
        $data['skipFields'] = $plugin->getSkipFields();
        $data['mappedFields'] = $plugin->getMappedFields();
        $data['countries'] = $plugin->getSourceData();
      }
    }
    return $data;
  }

}

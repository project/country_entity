<?php

namespace Drupal\country_entity\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Country entity entities.
 *
 * @ingroup country_entity
 */
class CountryEntityDeleteForm extends ContentEntityDeleteForm {


}

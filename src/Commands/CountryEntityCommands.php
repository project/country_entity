<?php

namespace Drupal\country_entity\Commands;

use Drupal\country_entity\CreateCountriesServiceInterface;
use Drush\Commands\DrushCommands;

/**
 * A Drush commandfile.
 *
 * In addition to this file, you need a drush.services.yml
 * in root of your module, and a composer.json file that provides the name
 * of the services file to use.
 *
 * See these files for an example of injecting Drupal services:
 *   - http://cgit.drupalcode.org/devel/tree/src/Commands/DevelCommands.php
 *   - http://cgit.drupalcode.org/devel/tree/drush.services.yml
 */
class CountryEntityCommands extends DrushCommands {

  protected $createCountries;

  protected $formatterManager;

  public function __construct(CreateCountriesServiceInterface $createCountries) {
    $this->createCountries = $createCountries;
  }

  /**
   * Import list of countries from data source.
   *
   * @param $source
   *   The datasource plugin of countries.
   * @usage country_entity-importCountry ic
   *   Imports countries into the country entity
   *
   * @command country_entity:importCountries
   * @aliases ic
   */
  public function importCountries(string $source) {
    $result = $this->createCountries->create($source);
    if ($result) {
      $this->logger()->success(dt('Countries Imported.'));
    }
    else {
      $this->logger()->success(dt('Countries Import not successful, check source id is correct.'));
    }
  }

  /**
   * Update list of countries from data source, based on ISO2 code.
   *
   * @param $arg1
   *   The datasource plugin of countries.
   * @usage country_entity-updateCountry uc
   *   Imports countries into the country entity
   * @options the target field used to update the country list.
   * @command country_entity:updateCountries
   * @aliases uc
   */
  public function updateCountries(string $source, array $options = ['field' => 'iso2']) {
    $this->logger()->success(dt('Countries Imported.'));
  }


  /**
   * Check for available source plugins.
   *
   * @usage country_entity-checkCountrySourceId ic
   *   Check for available sourceids.
   * @command country_entity:checkCountrySourceIds
   * @aliases ccc
   */
  public function checkCountrySourceIds() {
    $sourceId = $this->createCountries->getPluginIds();
    foreach ($sourceId as $source) {
      $this->logger()->success($source);
    }
  }

}

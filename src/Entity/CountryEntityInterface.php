<?php

namespace Drupal\country_entity\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;

/**
 * Provides an interface for defining Country entity entities.
 *
 * @ingroup country_entity
 */
interface CountryEntityInterface extends ContentEntityInterface, EntityChangedInterface, EntityPublishedInterface {

  /**
   * Add get/set methods for your configuration properties here.
   */

  /**
   * Gets the Country entity name.
   *
   * @return string
   *   Name of the Country entity.
   */
  public function getName();

  /**
   * Sets the Country entity name.
   *
   * @param string $name
   *   The Country entity name.
   *
   * @return \Drupal\country_entity\Entity\CountryEntityInterface
   *   The called Country entity entity.
   */
  public function setName($name);

  /**
   * Gets the Country entity creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Country entity.
   */
  public function getCreatedTime();

  /**
   * Sets the Country entity creation timestamp.
   *
   * @param int $timestamp
   *   The Country entity creation timestamp.
   *
   * @return \Drupal\country_entity\Entity\CountryEntityInterface
   *   The called Country entity entity.
   */
  public function setCreatedTime($timestamp);

}

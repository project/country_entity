<?php

namespace Drupal\country_entity;

/**
 * Interface CreateCountriesServiceInterface.
 */
interface CreateCountriesServiceInterface {

  /**
   * @param $datasource
   *
   * @return mixed
   */
  public function create($datasource);

}

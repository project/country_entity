<?php

namespace Drupal\country_entity\Tests;

use Drupal\country_entity\CreateCountriesServiceInterface;
use Drupal\Tests\BrowserTestBase;

/**
 * Provides automated tests for the country_entity module.
 */
class ImportControllerTest extends BrowserTestBase {

  /**
   * Drupal\country_entity\CreateCountriesServiceInterface definition.
   *
   * @var \Drupal\country_entity\CreateCountriesServiceInterface
   */
  protected $countryEntityCreateCountries;


  /**
   * {@inheritdoc}
   */
  public static function getInfo() {
    return [
      'name' => "country_entity ImportController's controller functionality",
      'description' => 'Test Unit for module country_entity and controller ImportController.',
      'group' => 'Other',
    ];
  }

  /**
   *
   */
  protected function setUp() {
    parent::setUp();
  }

  /**
   * Tests country_entity functionality.
   */
  public function testImportController() {
    // Check that the basic functions of module country_entity.
    $this->assertEquals(TRUE, TRUE, 'Test Unit Generated via Drupal Console.');
  }

}
